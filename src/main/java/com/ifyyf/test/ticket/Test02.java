package com.ifyyf.test.ticket;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-11-04 下午 07:19
 */
public class Test02 {
    public static void main(String[] args) throws InterruptedException {
        Test.Ticket ticket = new Test.Ticket();

        new Thread(()->{
            for(int i=0;i<100;i++){
                ticket.sale();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"A").start();

        new Thread(()->{
            for(int i=0;i<100;i++){
                ticket.sale();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"B").start();

        new Thread(()->{
            for(int i=0;i<100;i++){
                ticket.sale();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"C").start();
    }
    static class Ticket{
        private int num=100;
        //可重入锁
        Lock lock=new ReentrantLock();
        public void sale(){
            //上锁
            lock.lock();

            try{
                //do something 业务代码
                if(num<=0)return;
                System.out.println(Thread.currentThread().getName()+" 买到第"+(num--)+"张票，剩下"+num+"张票");
            }catch(Exception e){
                e.printStackTrace();
            }finally{
                lock.unlock();
            }
        }
    }
}
