package com.ifyyf.test.stream;


import java.util.Arrays;
import java.util.List;

/**
 * @Author if
 * @Description: stream流学习
 * 现在有5个用户进行筛选:
 * 1、ID必须是偶数
 * 2、年龄必须大于23岁
 * 3、用户名转为大写字母
 * 4、用户名字母倒着排序
 * 5、只输出一个用户
 * @Date 2021-11-07 下午 06:12
 */
public class Test01 {
    public static void main(String[] args) {
        User user1 = new User(1,"a",21);
        User user2 = new User(2,"b",22);
        User user3 = new User(3,"c",23);
        User user4 = new User(4,"d",24);
        User user5 = new User(6,"e",25);

        //将5个user转到list中
        List<User> list= Arrays.asList(user1,user2,user3,user4,user5);
        //计算交给stream流
        list.stream()
                //ID必须是偶数
                .filter(user-> user.getId()%2==0)
                //年龄必须大于23岁
                .filter(user-> user.getAge()>23)
                //用户名转为大写字母
                .map(user -> user.getName().toUpperCase())
                //用户名字母倒着排序(可简化为Comparator.reverseOrder())
                .sorted((u1,u2)->u2.compareTo(u1))
                //只输出一个用户
                .limit(1)
                .forEach(System.out::println);
    }
}
