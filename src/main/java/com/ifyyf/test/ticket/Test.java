package com.ifyyf.test.ticket;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-11-04 上午 11:12
 */
public class Test {
    public static void main(String[] args) throws InterruptedException {

        Ticket ticket = new Ticket();

        new Thread(()->{
            for(int i=0;i<100;i++){
                ticket.sale();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"A").start();

        new Thread(()->{
            for(int i=0;i<100;i++){
                ticket.sale();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"B").start();

        new Thread(()->{
            for(int i=0;i<100;i++){
                ticket.sale();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"C").start();
    }
    static class Ticket{
        private int num=100;
        public synchronized void sale(){
            if(num<=0)return;
            System.out.println(Thread.currentThread().getName()+" 买到第"+(num--)+"张票，剩下"+num+"张票");
        }
    }
}
