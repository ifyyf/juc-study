package com.ifyyf.test.unsafe;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @Author if
 * @Description: 不安全的Set
 * 解决方案：
 * 1.Set<String> synchronizedSet = Collections.synchronizedSet(set);
 * 2.Set<String> set = new CopyOnWriteArraySet<>();
 *
 * @Date 2021-11-05 下午 03:11
 */
public class SetTest {
    public static void main(String[] args) {

        //并发修改异常：java.util.ConcurrentModificationException
//        HashSet<String> set = new HashSet<>();
//        for(int i=1;i<=100;i++){
//            new Thread(()->{
//                set.add(UUID.randomUUID().toString().substring(0,5));
//                System.out.println(set);
//            },i+"").start();
//        }

//        HashSet<String> set = new HashSet<>();
//        Set<String> synchronizedSet = Collections.synchronizedSet(set);
//        for(int i=1;i<=100;i++){
//            new Thread(()->{
//                synchronizedSet.add(UUID.randomUUID().toString().substring(0,5));
//                System.out.println(synchronizedSet);
//            },i+"").start();
//        }

        Set<String> set = new CopyOnWriteArraySet<>();
        for(int i=1;i<=100;i++){
            new Thread(()->{
                set.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(set);
            },i+"").start();
        }
    }
}
