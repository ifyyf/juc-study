package com.ifyyf.test.bq;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-11-06 下午 01:16
 */
public class Test {
    public static void main(String[] args) throws InterruptedException {
//        throwException();
//        noExceptionAndReturn();
//        blockWait();
        outTimeWait();
    }

    //1、抛出异常
    public static void throwException(){
        //参数capacity表示队列大小
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);
        System.out.println(blockingQueue.add("a"));
        System.out.println(blockingQueue.add("b"));
        System.out.println(blockingQueue.add("c"));

        //此时队首为a
        System.out.println("blockingQueue.element() = " + blockingQueue.element());

        //队列大小设定为3，此时添加了第4个元素时会抛出异常
        //java.lang.IllegalStateException: Queue full
//        System.out.println(blockingQueue.add("d"));

        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());

        //清空队列后，再remove抛出异常java.util.NoSuchElementException
//        System.out.println(blockingQueue.remove());

        //清空队列后抛出NoSuchElementException
        System.out.println("blockingQueue.element() = " + blockingQueue.element());
    }

    //2.不抛出异常且有返回值
    public static void noExceptionAndReturn(){
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);
        System.out.println(blockingQueue.offer("a"));
        System.out.println(blockingQueue.offer("b"));
        System.out.println(blockingQueue.offer("c"));

        //此时队首为a
        System.out.println("blockingQueue.peek() = " + blockingQueue.peek());

        //队列满返回false
        System.out.println(blockingQueue.offer("d"));

        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());

        //队列空，返回null
        System.out.println(blockingQueue.poll());

        //队列空，无队首，返回null
        System.out.println("blockingQueue.peek() = " + blockingQueue.peek());
    }

    //3、阻塞等待
    public static void blockWait() throws InterruptedException {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);

        System.out.println("开始put");

        new Thread(()->{
            try {
                blockingQueue.put("a");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(()->{
            try {
                blockingQueue.put("b");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(()->{
            try {
                blockingQueue.put("c");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        //给1秒时间让线程插满队列
        TimeUnit.SECONDS.sleep(1);

        //此时队列已满，这个阻塞线程将被阻塞无法输出
        new Thread(()->{
            try {
                blockingQueue.put("d");
                System.out.println(Thread.currentThread().getName()+"put完毕");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"阻塞线程").start();

        //救急线程将bq中take出一个元素，队列空一个位置，上一个阻塞线程可以完成put
        new Thread(()->{
            try {
                blockingQueue.take();
                System.out.println("再开一个"+Thread.currentThread().getName()+"take出来，阻塞线程能不能输出？可以");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"救急线程").start();

        System.out.println("主线程能不能输出？能输出，不影响，因为阻塞的是上上面的那个阻塞线程");
        System.out.println("如果在主线程put的话，这里一样会被阻塞\n====================");
    }

    //4、超时等待
    public static void outTimeWait() throws InterruptedException {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);
        //用的还是offer和poll方法，不过这次是带参数的

        blockingQueue.offer("a");
        blockingQueue.offer("b");
        blockingQueue.offer("c");

        System.out.println("普通offer方法，立即放弃->"+blockingQueue.offer("d"));

        //等待3秒后，若还是阻塞则放弃
        //如果是不带参的方法会立即放弃
        System.out.println("带参offer方法，等待后放弃->"+blockingQueue.offer("d",3,TimeUnit.SECONDS));

        blockingQueue.poll();
        blockingQueue.poll();
        blockingQueue.poll();

        System.out.println("普通poll方法，立即放弃->" + blockingQueue.poll());

        //等待3秒后，还是阻塞则放弃
        System.out.println("带参poll方法，等待后放弃->" + blockingQueue.poll(3,TimeUnit.SECONDS));
    }
}
