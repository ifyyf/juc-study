package com.ifyyf.test.pc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-11-04 下午 11:44
 */
public class Test02 {
    public static void main(String[] args) throws InterruptedException {
        Data data = new Data();
        new Thread(()->{
            for(int i=0;i<100;i++){
                try {
                    data.pro();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },"ProducerA").start();

        new Thread(()->{
            for(int i=0;i<100;i++){
                try {
                    data.pro();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },"ProducerB").start();

        new Thread(()->{
            for(int i=0;i<100;i++){
                try {
                    data.con();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },"ConsumerA").start();

        new Thread(()->{
            for(int i=0;i<100;i++){
                try {
                    data.con();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },"ConsumerB").start();
    }
    static class Data{
        private int num=0;

        //获取锁
        Lock lock=new ReentrantLock();
        //获取condition取代synchronized
        Condition condition = lock.newCondition();

        public void pro(){
            lock.lock();
            try{

                while (num!=0){
                    System.out.println(Thread.currentThread().getName()+"正在等待");
                    //等待
                    condition.await();
                }
                num++;
                System.out.println(Thread.currentThread().getName()+" 生产者生产了一条消息，此时num="+num);
                //唤醒
                condition.signalAll();

            }catch(Exception e){
                e.printStackTrace();
            }finally{
                lock.unlock();
            }

        }
        public void con(){
            lock.lock();
            try{
                while (num==0){
                    System.out.println(Thread.currentThread().getName()+"正在等待");
                    condition.await();
                }
                num--;
                System.out.println(Thread.currentThread().getName()+" 消费者消费了一条消息，此时num="+num);
                condition.signalAll();
            }catch(Exception e){
                e.printStackTrace();
            }finally{
                lock.unlock();
            }

        }
    }
}
