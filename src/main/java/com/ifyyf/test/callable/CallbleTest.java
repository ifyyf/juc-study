package com.ifyyf.test.callable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-11-05 下午 09:10
 */
public class CallbleTest {
    public static void main(String[] args) {
        //老版方式：线程类继承Thread重写run方法，然后启动
//        new MyThread().start();
        //老版方式：线程类实现Runnable的run，将实例化对象放入Thread参数启动线程
//        new Thread(new MyRun()).start();

        /**
         * 使用Callable
         * 线程类实现Callable实现call()方法，这个方法可以有返回值，就是定义的泛型类型
         * 创建FutureTask，将线程类对象传入
         * 将FutureTask对象传入Thread再启动
         *
         * Thread(Runnable target)
         * FutureTask<V> implements RunnableFuture<V>
         * RunnableFuture<V> extends Runnable, Future<V>
         */
        MyCall callable = new MyCall();
        //适配类
        FutureTask<Integer> futureTask = new FutureTask<>(callable);
        //如果此时用同一个futureTask对象开启两条线程会有什么结果呢？
        //答案是：只会有一个线程进行运行，且只输出一次结果
        //因为FutureTask的state的状态从初始化NEW变了完成状态COMPLETING
        //然后在run方法中判断不为NEW则直接返回不执行了
        new Thread(futureTask,"A").start();
        new Thread(futureTask,"B").start();
        //获取返回结果，可能会抛异常
        //可能会阻塞，因为要等待执行完毕，所以最好把他放在最后，或者异步通信来处理
        try {
            Integer integer = futureTask.get();
            System.out.println(integer);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }

    static class MyCall implements Callable<Integer>{
        @Override
        public Integer call() throws Exception {
            System.out.println(Thread.currentThread().getName()+"class MyCall implements Callable<Integer>");
            return 1024;
        }
    }

    static class MyRun implements Runnable{
        @Override
        public void run() {
            System.out.println("class MyRun implements Runnable");
        }
    }

    static class MyThread extends Thread{
        @Override
        public void run() {
            System.out.println("class MyThread extends Thread");
        }
    }
}
