package com.ifyyf.test.lock8;

import java.util.concurrent.TimeUnit;

/**
 * @Author if
 * @Description:
 *
 * 问题：此时mail是静态synchronized方法，call是普通synchronized方法，问执行顺序？
 * 答题：
 * 先打电话再发邮件，因为两把锁不同，mail虽然先start，但是他锁住class且睡眠4秒
 * 此时main线程的1秒已经结束，开始call的start，而call锁的对象锁，和mail的class锁不同
 * 所以不需要同步，则执行call
 *
 * @Date 2021-11-05 上午 12:46
 */
public class Test4 {
    public static void main(String[] args) {

        Phone phone = new Phone();

        new Thread(()->{
            phone.mail();
        },"A").start();

        try{
            //main线程休眠
            TimeUnit.SECONDS.sleep(1);
        }catch(Exception e){
            e.printStackTrace();
        }

        new Thread(()->{
            phone.call();
        },"B").start();


    }

    static class Phone{

        public static synchronized void mail(){
            try{
                TimeUnit.SECONDS.sleep(4);
            }catch(Exception e){
                e.printStackTrace();
            }
            System.out.println("发邮件");
        }
        public synchronized void call(){
            System.out.println("打电话");
        }
    }
}
