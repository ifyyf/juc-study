package com.ifyyf.test.lock8;

import java.util.concurrent.TimeUnit;

/**
 * @Author if
 * @Description:
 *
 * 问题：mail和call方法改为static静态方法后，调用顺序
 * 答题：先短信再电话，因为此时锁住的Phone.class，而不是实例化对象
 *
 * 问题：实例化两个对象，phone1和phone2，调用顺序
 * 答题：还是先短信再电话，锁住的是Phone.class，而不是实例化对象
 *
 *
 * @Date 2021-11-05 上午 12:37
 */
public class Test3 {
    public static void main(String[] args) {

        Phone phone = new Phone();

        new Thread(()->{
            phone.mail();
        },"A").start();

        try{
            //main线程休眠
            TimeUnit.SECONDS.sleep(1);
        }catch(Exception e){
            e.printStackTrace();
        }

        new Thread(()->{
            phone.call();
        },"B").start();


        Phone phone1 = new Phone();
        Phone phone2 = new Phone();

        new Thread(()->{
            phone1.mail();
        },"A").start();

        try{
            //main线程休眠
            TimeUnit.SECONDS.sleep(1);
        }catch(Exception e){
            e.printStackTrace();
        }

        new Thread(()->{
            phone2.call();
        },"B").start();

    }

     static class Phone{
        //synchronized锁住class，和对象相不相同没有关系，因为多个对象的class都是这同一个
        public static synchronized void mail(){
            try{
                TimeUnit.SECONDS.sleep(4);
            }catch(Exception e){
                e.printStackTrace();
            }
            System.out.println("发邮件");
        }
        public static synchronized void call(){
            System.out.println("打电话");
        }
    }
}
