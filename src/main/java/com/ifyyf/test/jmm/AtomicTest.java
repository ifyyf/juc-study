package com.ifyyf.test.jmm;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-11-08 下午 11:12
 */
public class AtomicTest {
    private volatile static AtomicInteger num=new AtomicInteger(0);

    public static void add(){
        //自增并返回，CAS乐观锁
        num.getAndIncrement();
    }

    public static void main(String[] args) {
        long startTime=System.currentTimeMillis();
        for (int i = 0; i < 20000; i++) {
            new Thread(()->{
                for (int j = 0; j < 10000; j++) {
                    add();
                }
            }).start();
        }
        while(Thread.activeCount()>2){
            Thread.yield();
        }
        long endTime=System.currentTimeMillis();
        System.out.println("程序运行时间： "+(endTime-startTime)+"ms");
        //获得num的值并输出
        System.out.println("num.get() = "+num.get());
    }
}
