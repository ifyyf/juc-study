package com.ifyyf.test.future;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @Author if
 * @Description:
 * 异步调用
 * 成功回调
 * 失败回调
 * @Date 2021-11-07 下午 11:16
 */
public class FutureDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //没有返回值的runAsync异步调用
        CompletableFuture<Void> runAsync = CompletableFuture.runAsync(() -> {
            System.out.println("CompletableFuture.runAsync");
        });
        runAsync.get();

        System.out.println("==============");

        //有返回值的runAsync异步调用
        //返回一个CompletableFuture类对象，用get获取最终结果
        CompletableFuture<Integer> result = CompletableFuture.supplyAsync(() -> {
            //执行的业务逻辑
            System.out.println("CompletableFuture.supplyAsync中执行业务逻辑");
//            int i=1/0;
            return 200;
        })
            //成功回调，无论是否异常都执行，所以判断t和u来决定代码逻辑
            .whenComplete((t, u) -> {
                //t不为null则正常执行
                if(!Objects.isNull(t)&&Objects.isNull(u)){
                    System.out.println("t = " + t);//正常的返回结果
                }else{
                    //u为null则正常执行，否则就是异常信息
                    System.out.println("异常信息u = " + u);//错误信息
                }
             })
            //失败回调，只有在出现异常时才会执行
            .exceptionally((e) -> {
                //这里的参数一般是Exception e
//                e.printStackTrace();
                System.out.println("异常回调->e.getMessage() = " + e.getMessage());
                return 400;
            });

        //获取  成功/异常  的结果
        System.out.println("result.get() = "+result.get());

    }
}
