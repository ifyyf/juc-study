package com.ifyyf.test.pc;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-11-04 下午 10:39
 */
public class Test01 {
    public static void main(String[] args) throws InterruptedException {
        Data data = new Data();
        new Thread(()->{
            for(int i=0;i<100;i++){
                try {
                    data.pro();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"ProducerA").start();

        new Thread(()->{
            for(int i=0;i<100;i++){
                try {
                    data.pro();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"ProducerB").start();

        new Thread(()->{
            for(int i=0;i<100;i++){
                try {
                    data.con();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"ConsumerA").start();

        new Thread(()->{
            for(int i=0;i<100;i++){
                try {
                    data.con();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"ConsumerB").start();
    }
    static class Data{
        private int num=0;

        public synchronized void pro() throws InterruptedException {
            while (num!=0){
                System.out.println(Thread.currentThread().getName()+"正在等待");
                this.wait();
            }
            num++;
            System.out.println(Thread.currentThread().getName()+" 生产者生产了一条消息，此时num="+num);
            this.notifyAll();
        }
        public synchronized void con() throws InterruptedException {
            while (num==0){
                System.out.println(Thread.currentThread().getName()+"正在等待");
                this.wait();
            }
            num--;
            System.out.println(Thread.currentThread().getName()+" 消费者消费了一条消息，此时num="+num);
            this.notifyAll();
        }
    }
}
