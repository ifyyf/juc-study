package com.ifyyf.test.function;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @Author if
 * @Description: 消费型接口和供给型接口
 * @Date 2021-11-07 下午 05:59
 */
public class Test02 {
    public static void main(String[] args) {
        Consumer<String> consumer= str-> System.out.println(str);
        consumer.accept("out");

        Consumer<String> consumerS= System.out::println;
        consumerS.accept("out");


        Supplier<String> supplier= ()-> "asd";
        System.out.println(supplier.get());
    }
}
