package com.ifyyf.test.lock8;

import java.util.concurrent.TimeUnit;

/**
 * @Author if
 * @Description:
 *
 * 问题：如果我将mail方法上synchronized，call方法不上，此时调用顺序
 * 答题：先打电话再发邮件，因为call不需要获得锁就能执行，只是需要等待main线程的1秒睡眠
 *
 * @Date 2021-11-05 上午 12:29
 */
public class Test2 {
    public static void main(String[] args) {
        Phone phone = new Phone();
        /**
         * 问题：假如现在有两个phone，且mail和call方法都是synchronized时，调用俩对象的方法时的执行顺序
         * 答题：当然是按正常顺序来了，因为synchronized锁的调用者，即实例化对象，而这有两个不同的对象
         */
//        Phone phone1 = new Phone();
//        Phone phone2 = new Phone();
//        new Thread(()->{
//            phone1.mail();
//        },"A").start();
//        try{
//            //main线程休眠
//            TimeUnit.SECONDS.sleep(1);
//        }catch(Exception e){
//            e.printStackTrace();
//        }
//        new Thread(()->{
//            phone2.call();
//        },"B").start();


        new Thread(()->{
            phone.mail();
        },"A").start();

        try{
            //main线程休眠
            TimeUnit.SECONDS.sleep(1);
        }catch(Exception e){
            e.printStackTrace();
        }

        new Thread(()->{
            phone.call();
        },"B").start();
    }

    static class Phone{
        //synchronized锁住调用者（实例化的对象）
        public synchronized void mail(){
            try{
                //休眠了锁也没被释放
                TimeUnit.SECONDS.sleep(4);
            }catch(Exception e){
                e.printStackTrace();
            }
            System.out.println("发邮件");
        }
        //这里没有上同步锁，当然不受影响了
        public void call(){
            System.out.println("打电话");
        }
    }
}
