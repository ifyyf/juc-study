package com.ifyyf.test.add;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * @Author if
 * @Description: 信号量
 * 参数permits许可证，可以用于限制线程数
 * @Date 2021-11-06 上午 12:17
 */
public class SemaphoreDemo {
    public static void main(String[] args) {
        //permits许可证
        //我们这假设有3个车位和6辆车需要停车
        Semaphore semaphore = new Semaphore(3);

        for(int i=1;i<=6;i++){
            new Thread(()->{
                try{
                    //acquire得到许可证
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName()+"线程抢到车位");
                    TimeUnit.SECONDS.sleep(3);
                    System.out.println(Thread.currentThread().getName()+"线程离开车位");
                }catch(Exception e){
                    e.printStackTrace();
                }finally{
                    //release释放许可证
                    semaphore.release();
                }
            },i+"").start();
        }
    }
}
