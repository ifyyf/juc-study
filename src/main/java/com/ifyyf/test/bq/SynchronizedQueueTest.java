package com.ifyyf.test.bq;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/**
 * @Author if
 * @Description: 同步队列
 * 和其他的BlockingQueue不同，他不是用于储存元素
 * put了一个元素后就必须去take出来，不然就会等待（相当于只有1个空间的BlockingQueue？）
 * @Date 2021-11-06 下午 04:42
 */
public class SynchronizedQueueTest {
    public static void main(String[] args) {
        BlockingQueue<String> synchronousQueue = new SynchronousQueue<>();

        new Thread(()->{
            try {
                System.out.println(Thread.currentThread().getName()+"准备put a");
                synchronousQueue.put("a");

                System.out.println(Thread.currentThread().getName()+"准备put b");
                synchronousQueue.put("b");

                System.out.println(Thread.currentThread().getName()+"准备put c");
                synchronousQueue.put("c");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        },"Thead-put").start();

        new Thread(()->{
            try {
                //给一点让他put的时间
                TimeUnit.SECONDS.sleep(1);
                System.out.println(Thread.currentThread().getName()+"take = " + synchronousQueue.take());

                TimeUnit.SECONDS.sleep(1);
                System.out.println(Thread.currentThread().getName()+"take = " + synchronousQueue.take());

                TimeUnit.SECONDS.sleep(1);
                System.out.println(Thread.currentThread().getName()+"take = " + synchronousQueue.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"Thread-take").start();
    }
}
