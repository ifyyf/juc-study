package com.ifyyf.test.pc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author if
 * @Description: 使用Condition实现精准唤醒
 * @Date 2021-11-05 上午 12:04
 */
public class Test03 {
    public static void main(String[] args) {
        Data data=new Data();
        new Thread(()->{
            for(int i=0;i<10;i++){
                data.soutA();
            }
        },"A").start();
        new Thread(()->{
            for(int i=0;i<10;i++){
                data.soutB();
            }
        },"B").start();
        new Thread(()->{
            for(int i=0;i<10;i++){
                data.soutC();
            }
        },"C").start();
    }
    static class Data{

        private Lock lock=new ReentrantLock();
        private Condition condition1=lock.newCondition();
        private Condition condition2=lock.newCondition();
        private Condition condition3=lock.newCondition();
        private int num=1;

        public void soutA(){
            lock.lock();
            try{
                //num不为1时，condition1等待
                while(num!=1){
                    condition1.await();
                }
                System.out.println("AAAAAA");
                num=2;
                //A输出完后，唤醒condition2来输出B
                condition2.signal();
            }catch(Exception e){
                e.printStackTrace();
            }finally{
                lock.unlock();
            }
        }

        public void soutB(){
            lock.lock();
            try{
                //num不为2时，condition2等待
                while(num!=2){
                    condition2.await();
                }
                System.out.println("BBBBBB");
                num=3;
                //B输出完后，唤醒condition3来输出C
                condition3.signal();
            }catch(Exception e){
                e.printStackTrace();
            }finally{
                lock.unlock();
            }
        }

        public void soutC(){
            lock.lock();
            try{
                //num不为3时，condition3等待
                while(num!=3){
                    condition3.await();
                }
                System.out.println("CCCCCC");
                num=1;
                //B输出完后，唤醒condition1来输出A
                condition1.signal();
            }catch(Exception e){
                e.printStackTrace();
            }finally{
                lock.unlock();
            }
        }
    }
}
