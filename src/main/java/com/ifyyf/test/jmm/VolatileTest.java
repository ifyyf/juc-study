package com.ifyyf.test.jmm;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-11-08 下午 06:27
 */
public class VolatileTest {

    private static int num=0;
    private static volatile int vnum=0;

    public static void add(){
        num++;
        vnum++;
    }

    private static int snum=0;
    public synchronized static void sAdd(){
        snum++;
    }

    private static int lnum=0;
    private static Lock lock=new ReentrantLock();
    public static void lAdd(){
        lock.lock();
        try{
            lnum++;
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        long startTime=System.currentTimeMillis();
        for (int i = 0; i < 20000; i++) {
            new Thread(()->{
                for (int j = 0; j < 10000; j++) {
//                    add();
                    sAdd();
//                    lAdd();
                }
            }).start();
        }

        while (Thread.activeCount()>2){
            //线程让出当前时间片给其他线程执行
            Thread.yield();
        }
        long endTime=System.currentTimeMillis();
        System.out.println("程序运行时间： "+(endTime-startTime)+"ms");
//        System.out.println("结束,普通的num = "+num+",加了volatile的vnum = "+vnum);
        System.out.println("结束,synchronized方法的snum = "+snum);
//        System.out.println("结束,lock方法的lnum = "+lnum);
    }
}
