package com.ifyyf.test.add;

import java.util.concurrent.CountDownLatch;

/**
 * @Author if
 * @Description: 倒计时锁存器（减法计数器）
 * 每次调用countDown方法进行-1
 * 当总数不为0时，会一直阻塞下去
 * 可以用于线程的强制执行（因为不执行会阻塞）
 * @Date 2021-11-06 上午 12:00
 */
public class CountDownLatchDemo {
    public static void main(String[] args) throws InterruptedException {
        //count总数，用于倒计时
        //配合await方法，在倒计时结束前不会再向下执行代码
        CountDownLatch countDownLatch = new CountDownLatch(12);

        for(int i=1;i<=6;i++){
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("第"+i+"次执行countDown");
            countDownLatch.countDown();
//            new Thread(()->{
//                System.out.println(Thread.currentThread().getName()+"线程走了");
//                //数量-1
//                countDownLatch.countDown();
//            },i+"").start();
        }
        //等待计数器归零，然后再向下执行
        countDownLatch.await();
        System.out.println("关门");
    }
}
