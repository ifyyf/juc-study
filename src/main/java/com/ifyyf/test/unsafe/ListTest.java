package com.ifyyf.test.unsafe;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @Author if
 * @Description: 不安全的List
 * 这些集合单线程下是安全的，但是多线程下呢？
 * 解决方案：
 * 1.List<String> stringList=new Vector<>()
 *      底层加了synchronized
 * 2.List<String> synchronizedList=Collections.synchronizedList(new ArrayList<>())
 *      将普通list转为线程安全的list
 * 3.List<String> list = new CopyOnWriteArrayList<>()
 *      private transient volatile Object[] array;
 *      lock.lock()和lock.unlock()
 *
 * @Date 2021-11-05 下午 02:24
 */
public class ListTest {
    public static void main(String[] args) {

        //多线程下的ArrayList插入报错
        // 并发修改异常：java.util.ConcurrentModificationException
//        List<String> stringList=new ArrayList<>();
//        for(int i=1;i<=100;i++){
//            new Thread(()->{
//                stringList.add(UUID.randomUUID().toString().substring(0,5));
//                System.out.println(stringList);
//            },i+"").start();
//        }

        //多线程下的Vector没有报错，因为底层加了synchronized
//        List<String> stringVector=new Vector<>();
//        for(int i=1;i<=100;i++){
//            new Thread(()->{
//                stringVector.add(UUID.randomUUID().toString().substring(0,5));
//                System.out.println(stringVector);
//            },i+"").start();
//        }

        //如果我想用安全的list呢
        //使用Collections.synchronizedList将普通list转为线程安全的list
//        List<String> stringList=new ArrayList<>();
//        List<String> synchronizedList = Collections.synchronizedList(stringList);
//        for(int i=1;i<=100;i++){
//            new Thread(()->{
//                synchronizedList.add(UUID.randomUUID().toString().substring(0,5));
//                System.out.println(synchronizedList);
//            },i+"").start();
//        }

        /**
         * JUC：使用CopyOnWriteArrayList，解决并发
         * COW ：写入时复制，一种优化策略
         * list是唯一固定的，多个线程读取时是固定的，但是写入时有可能会覆盖
         * COW写入时避免了覆盖，防止了数据问题
         * 写入时先复制一份长度+1的数组，然后末尾插入数据，再把数组赋给原数组完成插入
         *
         * CopyOnWriteArrayList比vector好在哪？
         * Vector 的曾删改查方法都加上了synchronized锁，保证同步的情况下，因为每个方法都要去获得锁，所以性能就会大大下降
         *
         * CopyOnWriteArrayList 方法只是在增删改方法上增加了ReentrantLock锁
         * 但是他的读方法不加锁，读写分离，所以在读的方面就要比Vector性能要好，CopyOnWriteArrayList适合读多写少的并发情况
         */
        List<String> list = new CopyOnWriteArrayList<>();
        for(int i=1;i<=100;i++){
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(list);
            },i+"").start();
        }
    }
}
