package com.ifyyf.test.add;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @Author if
 * @Description: 循环屏障（加法计数器）
 * 使用await方法阻塞当前线程
 * 当执行的await方法次数达不到构造器传入的参数parties时，会一直阻塞下去
 * @Date 2021-11-06 上午 12:08
 */
public class CyclicBarrierDemo {
    public static void main(String[] args) throws InterruptedException, BrokenBarrierException {
        //集齐7颗龙珠召唤神龙
        CyclicBarrier cyclicBarrier = new CyclicBarrier(7,()->{
            System.out.println("集齐7颗龙珠召唤神龙成功");
        });
        for(int i=1;i<=7;i++){
            Thread.sleep(500);
            int finalI = i;
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"收集了第"+finalI+"颗龙珠");
                try {
                    //执行等待，直到等待了7次
                    cyclicBarrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
            },i+"").start();
        }
    }
}
