package com.ifyyf.test.unsafe;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author if
 * @Description: 不安全的Map
 * @Date 2021-11-05 下午 03:39
 */
public class MapTest {
    public static void main(String[] args) {

        //并发修改异常：ConcurrentModificationException
//        HashMap<String, String> map = new HashMap<>();
//        for(int i=1;i<=100;i++){
//            new Thread(()->{
//                map.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0,5));
//                System.out.println(map);
//            }).start();
//        }

//        Map<String, String> hashtable = new Hashtable<>();
//        for (int i = 1; i <= 100; i++) {
//            new Thread(() -> {
//                hashtable.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0, 5));
//                System.out.println(hashtable);
//            }).start();
//        }

//        HashMap<String, String> map = new HashMap<>();
//        Map<String, String> synchronizedMap = Collections.synchronizedMap(map);
//        for (int i = 1; i <= 100; i++) {
//            new Thread(() -> {
//                synchronizedMap.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0, 5));
//                System.out.println(synchronizedMap);
//            }).start();
//        }

        Map<String, String> concurrentHashMap = new ConcurrentHashMap<>();
        for (int i = 1; i <= 100; i++) {
            new Thread(() -> {
                concurrentHashMap.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0, 5));
                System.out.println(concurrentHashMap);
            }).start();
        }
    }
}
