package com.ifyyf.test.deadlock;

import java.util.concurrent.TimeUnit;

/**
 * @Author if
 * @Description: 死锁样例
 * @Date 2021-11-09 下午 06:54
 */
public class DeadLockDemo {
    public static void main(String[] args) {
        Object lockA=new Object();
        Object lockB=new Object();

        new Thread(()->{
            synchronized (lockA){
                System.out.println(Thread.currentThread().getName()+"获取到A锁");

                try{
                    TimeUnit.SECONDS.sleep(1);
                }catch(Exception e){
                    e.printStackTrace();
                }

                synchronized (lockB){
                    System.out.println(Thread.currentThread().getName()+"获取到B锁");
                }
            }
        },"A").start();

        new Thread(()->{
            synchronized (lockB){
                System.out.println(Thread.currentThread().getName()+"获取到B锁");

                try{
                    TimeUnit.SECONDS.sleep(1);
                }catch(Exception e){
                    e.printStackTrace();
                }

                synchronized (lockA){
                    System.out.println(Thread.currentThread().getName()+"获取到A锁");
                }
            }
        },"B").start();
    }
}
