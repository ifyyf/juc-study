package com.ifyyf.test.function;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @Author if
 * @Description: 函数型接口和断定型接口的学习
 * @Date 2021-11-07 下午 04:58
 */
public class Test01 {
    public static void main(String[] args) {
        //普通调用，使用匿名内部类
        Function<String,String> function=new Function<String, String>() {
            @Override
            public String apply(String s) {
                return s;
            }
        };
        System.out.println(function.apply("test"));

        //lambda
        Function<String,String> functionL= (str)->{return str;};
        System.out.println(functionL.apply("lambda"));

        //更简易
        Function<String,String> functionLS= str-> str;
        System.out.println(functionLS.apply("simple"));


        Predicate<Integer> predicate = new Predicate<Integer>(){
            @Override
            public boolean test(Integer num) {
                return num.equals(1);
            }
        };
        System.out.println(predicate.test(2));
        System.out.println(predicate.test(1));

        Predicate<Integer> predicateL= num-> num.equals(1);
        System.out.println("predicateL.test(1) = "+predicateL.test(1));

    }
}
