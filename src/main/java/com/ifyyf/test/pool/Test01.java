package com.ifyyf.test.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-11-06 下午 05:03
 */
public class Test01 {
    public static void main(String[] args) throws InterruptedException {

        //手动创建线程
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("手动创建线程");
            }
        },"name").start();


        //Single单个，创建单个线程处理
        ExecutorService service1 = Executors.newSingleThreadExecutor();
        try{
            for(int i=1;i<=5;i++){
                service1.execute(()->{
                    System.out.println(Thread.currentThread().getName()+"在执行");
                });
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            service1.shutdown();
            TimeUnit.SECONDS.sleep(1);
            System.out.println("========== service1已关闭 ==========");
        }

        //Fix固定，根据参数nThreads，创建固定的线程池大小
        ExecutorService service2 = Executors.newFixedThreadPool(5);
        try{
            for(int i=1;i<=10;i++){
                service2.execute(()->{
                    System.out.println(Thread.currentThread().getName()+"在执行");
                });
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            service2.shutdown();
            TimeUnit.SECONDS.sleep(1);
            System.out.println("========== service2已关闭 ==========");
        }


        //可伸缩的，遇强则强，遇弱则弱
        ExecutorService service3 = Executors.newCachedThreadPool();
        try{
            for(int i=1;i<=10;i++){
                //加入睡眠后发现全都是pool-3-thread-1在执行
                //推断可能是同时存在大量并发时，才会有多个线程入池
//                Thread.sleep(1);
                service3.execute(()->{
                    System.out.println(Thread.currentThread().getName()+"在执行");
                });
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            service3.shutdown();
            TimeUnit.SECONDS.sleep(1);
            System.out.println("========== service3已关闭 ==========");
        }
    }
}
