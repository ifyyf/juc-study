package com.ifyyf.test.lock8;

import java.util.concurrent.TimeUnit;

/**
 * @Author if
 * @Description:
 *
 * 问题：main主线程中  没有  添加TimeUnit.SECONDS.sleep(1);之前，输出顺序？
 * 答题：看cpu的时间片分配，随机的（多开几个线程测试就行）
 *
 * 问题：mail方法中  没有  添加TimeUnit.SECONDS.sleep(4);之前，输出顺序？
 * 答题：先发邮件再打电话
 *
 * 问题：mail方法中添加了TimeUnit.SECONDS.sleep(4);后，输出顺序？
 * 答题：还是先发邮件再打电话，因为synchronized不会释放锁，直到代码结束才释放
 *
 * @Date 2021-11-05 上午 12:20
 */
public class Test1 {

    public static void main(String[] args) {
        Phone phone = new Phone();
        new Thread(()->{
            phone.mail();
        },"A").start();

        try{
            //main线程休眠
            TimeUnit.SECONDS.sleep(1);
        }catch(Exception e){
            e.printStackTrace();
        }

        new Thread(()->{
            phone.call();
        },"B").start();
    }

    static class Phone{
        //synchronized锁住调用者（实例化的对象）
        public synchronized void mail(){
            try{
                //休眠了锁也没被释放
                TimeUnit.SECONDS.sleep(4);
            }catch(Exception e){
                e.printStackTrace();
            }
            System.out.println("发邮件");
        }
        public synchronized void call(){
            System.out.println("打电话");
        }
    }
}
